package ru.t1.didyk.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";
    @NotNull
    public static final String DESCRIPTION = "Show application version.";

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }
}
