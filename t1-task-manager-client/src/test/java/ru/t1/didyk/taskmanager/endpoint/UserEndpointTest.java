package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.endpoint.IUserEndpoint;
import ru.t1.didyk.taskmanager.constant.SharedTestMethods;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.service.PropertyService;

@Category(IntegrationCategory.class)
public class UserEndpointTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private String testUserLogin;

    @Nullable
    private String testUserPassword;

    @Nullable
    private String testUserEmail;

    @Before
    public void before() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserRegistryRequest request = new UserRegistryRequest(userToken);
        testUserLogin = "test-user" + SharedTestMethods.getRandomString(10);
        testUserPassword = "test-pass" + SharedTestMethods.getRandomString(10);
        testUserEmail = SharedTestMethods.getRandomString(10) + "@test.test";
        request.setLogin(testUserLogin);
        request.setPassword(testUserPassword);
        request.setEmail(testUserEmail);
        userEndpoint.register(request);
    }

    @After
    public void after() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserRemoveRequest request = new UserRemoveRequest(userToken);
        request.setLogin(testUserLogin);
        userEndpoint.removeUser(request);
    }

    @Test
    public void changePasswordTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("test-user", "test-user");
        @NotNull UserChangePasswordRequest request = new UserChangePasswordRequest(userToken);
        request.setPassword("newpass");
        @Nullable final UserChangePasswordResponse response = userEndpoint.changePassword(request);
        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getPasswordHash().isEmpty());
    }

    @Test
    public void lockUserTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserLockRequest request = new UserLockRequest(userToken);
        request.setLogin("test-user");
        @Nullable final UserLockResponse response = userEndpoint.lockUser(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void register() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserRegistryRequest request = new UserRegistryRequest(userToken);
        String login = "test-user" + SharedTestMethods.getRandomString(10);
        String password = "test-pass" + SharedTestMethods.getRandomString(10);
        String email = SharedTestMethods.getRandomString(10) + "@test.test";
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final UserRegistryResponse response = userEndpoint.register(request);
        @Nullable final User user = response.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void removeUserTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserRegistryRequest registryRequest = new UserRegistryRequest(userToken);
        String login = "test-user" + SharedTestMethods.getRandomString(10);
        String password = "test-pass" + SharedTestMethods.getRandomString(10);
        String email = SharedTestMethods.getRandomString(10) + "@test.test";
        registryRequest.setLogin(login);
        registryRequest.setPassword(password);
        registryRequest.setEmail(email);
        userEndpoint.register(registryRequest);

        @NotNull UserRemoveRequest removeRequest = new UserRemoveRequest(userToken);
        removeRequest.setLogin(login);
        @Nullable final UserRemoveResponse removeResponse = userEndpoint.removeUser(removeRequest);
        @Nullable final User user = removeResponse.getUser();
        Assert.assertNotNull(user);
        Assert.assertEquals(login, user.getLogin());
        Assert.assertEquals(email, user.getEmail());
    }

    @Test
    public void unlockUserTest() {
        @Nullable final String userToken = SharedTestMethods.loginUser("admin", "admin");
        @NotNull UserUnlockRequset request = new UserUnlockRequset(userToken);
        request.setLogin("test-user");
        @Nullable final UserUnlockResponse response = userEndpoint.unlockUser(request);
        Assert.assertNotNull(response);
    }

}