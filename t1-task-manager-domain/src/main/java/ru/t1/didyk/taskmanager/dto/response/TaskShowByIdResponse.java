package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.Task;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable Task task) {
        super(task);
    }
}
