package ru.t1.didyk.taskmanager.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable String token) {
        super(token);
    }
}
