package ru.t1.didyk.taskmanager.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.component.ISaltProvider;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.model.Session;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.service.PropertyService;
import ru.t1.didyk.taskmanager.util.HashUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectTestData {

    @NotNull
    public final static Project USER_PROJECT = new Project();

    @NotNull
    public final static Project USER_PROJECT2 = new Project();

    @NotNull
    public final static Project USER_PROJECT3 = new Project();

    @NotNull
    public final static List<Project> USER_PROJECT_LIST = new ArrayList<Project>();

    @NotNull
    public final static Project ADMIN_PROJECT = new Project();

    @NotNull
    public final static Task USER_TASK = new Task();

    @NotNull
    public final static Task USER_TASK2 = new Task();

    @NotNull
    public final static Task USER_TASK3 = new Task();

    @NotNull
    public final static Task ADMIN_TASK = new Task();

    @NotNull
    public final static List<Task> USER_TASK_LIST = new ArrayList<Task>();

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static Project NULL_PROJECT = null;

    @NotNull
    public final static Task NULL_TASK = null;

    @Nullable
    public final static String NULL_STRING = null;

    @NotNull
    public final static Session SESSION = new Session();

    static {
        @Nullable final ISaltProvider propertyService = new PropertyService();

        USER1.setLogin("Test1");
        USER1.setFirstName("Testing");
        USER1.setLastName("First");
        USER1.setRole(Role.USUAL);
        USER1.setEmail("test@email.com");
        USER1.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER2.setLogin("Test2");
        USER2.setFirstName("Testing");
        USER2.setLastName("Second");
        USER2.setRole(Role.ADMIN);
        USER2.setPasswordHash(HashUtil.salt(propertyService, "password"));

        USER_PROJECT.setUserId(USER1.getId());
        USER_PROJECT.setName("First");
        USER_PROJECT.setStatus(Status.IN_PROGRESS);
        USER_PROJECT.setDescription("project");

        USER_PROJECT2.setUserId(USER1.getId());
        USER_PROJECT2.setName("Second User");
        USER_PROJECT2.setStatus(Status.IN_PROGRESS);
        USER_PROJECT2.setDescription("project");

        USER_PROJECT3.setUserId(USER1.getId());
        USER_PROJECT3.setName("Third Project");
        USER_PROJECT3.setStatus(Status.IN_PROGRESS);
        USER_PROJECT3.setDescription("project");

        for (int i = 0; i < 3; i++) {
            Project project = new Project();
            project.setUserId(USER1.getId());
            project.setName("Collection Project " + i);
            project.setStatus(Status.NOT_STARTED);
            project.setDescription("Collection Project " + i);
            USER_PROJECT_LIST.add(project);

            Task task = new Task();
            task.setUserId(USER1.getId());
            task.setName("Collection Task " + i);
            task.setStatus(Status.NOT_STARTED);
            task.setDescription("Collection Task " + i);
            USER_TASK_LIST.add(task);
        }

        ADMIN_PROJECT.setUserId(USER2.getId());
        ADMIN_PROJECT.setName("Second");
        ADMIN_PROJECT.setStatus(Status.COMPLETED);
        ADMIN_PROJECT.setDescription("project");

        USER_TASK.setUserId(USER1.getId());
        USER_TASK.setName("First");
        USER_TASK.setStatus(Status.IN_PROGRESS);
        USER_TASK.setDescription("task");
        USER_TASK.setProjectId(USER_PROJECT.getId());

        USER_TASK2.setUserId(USER1.getId());
        USER_TASK2.setName("Second");
        USER_TASK2.setStatus(Status.IN_PROGRESS);
        USER_TASK2.setDescription("task");

        USER_TASK3.setUserId(USER1.getId());
        USER_TASK3.setName("Third");
        USER_TASK3.setStatus(Status.IN_PROGRESS);
        USER_TASK3.setDescription("task");

        ADMIN_TASK.setUserId(USER2.getId());
        ADMIN_TASK.setName("Second");
        ADMIN_TASK.setStatus(Status.COMPLETED);
        ADMIN_TASK.setDescription("task");

        SESSION.setUserId(USER1.getId());
        SESSION.setRole(Role.USUAL);
        SESSION.setDate(new Date());

    }


}
